
//1.-Se realiza la carga de los datos del json
    d3.json('https://gist.githubusercontent.com/josulazaroversia/619d17e343c9209e15e666e9d4a2c408/raw/4a6963cf4c6945b018a0197a2d6d75786a6b13fb/Datos_practica.json')
    .then((featureCollection) => {
        drawMap(featureCollection);
    });

//2.-Se crea la función para pintar el mapa completo

function drawMap(featureCollection) {
        
    //2.0. Definición de las variables y constantes que se van a usar en el programa
    const width = 600;
    const height = 600;

    //2.1 creacion SVG y agregamos atributos 
        const svg = d3.select('#prueba')
            .append('svg')
            .attr('width', width)
            .attr('height', height);          

    //2.2 Crea la proyección a visualizar

        //Definimos el centroide
        const center = d3.geoCentroid(featureCollection);

        //Creamos la variable proyección donde se pinta el mapa
        const projection =  d3.geoMercator()
            // .scale(100000)
            .fitSize([width, height], featureCollection)
            .center(center)
            .translate([width-260, height-300])   

        const pathProjection = d3.geoPath().projection(projection);

    //3 Nos quedamos con features que contiene la estructura de datos a tratar
        const features = featureCollection.features;

    //4.Declaramos variable grupo

        const groupMap = svg
            .append('g')
            .attr('class', 'map');

    //5.Escala de colores según precio
        //Definimos un minimo y máximo para configurar la escala de color
        
        const min= d3.min(features, function(d){ return d.properties.avgprice; });
        const max= d3.max(features, function(d){ return d.properties.avgprice; });

        color = d3.scaleLinear()
        .domain([min,max])
        .range(["rgb(249, 188, 174)", "rgb(243,51,10)"])
        .interpolate(d3.interpolateLab) 

            
       // const color = d3.scaleQuantize([1, 10], d3.schemeBlues[9]);
        //--Version original del color de la plantilla
        //const color = d3.scaleOrdinal(d3.schemeCategory10);
        //subunitsPath.attr('fill', (d) => color(d.properties.avgprice))

        /*var color = d3.scaleQuantile()
        .range(["rgb(237, 248, 233)", "rgb(186, 228, 179)", "rgb(116,196,118)", "rgb(49,163,84)", "rgb(0,109,44)"]);
    
        color.domain([ d3.min(features, function(d){ return d.properties.avgprice; }),
            d3.max(features, function(d){ return d.properties.avgprice; })
            ]);*/

    //6. Declaramos las funciones que desarrollan los eventos. Se ha dejado sin terminar pq no daba tiempo
    
    let mouseOver = function(d) {
        d3.selectAll(".d.properties.")
          .transition()
          .duration(200)
          .style("opacity", .5)
        d3.select(this)
          .transition()
          .duration(200)
          .style("opacity", 1)
          .style("stroke", "black")          
      }

      let mouseLeave = function(d) {
        d3.selectAll(".Country")
          .transition()
          .duration(200)
          .style("opacity", .8)
        d3.select(this)
          .transition()
          .duration(200)
          .style("stroke", "transparent")
      }

      let onclick = function (d) {
        //console.log(`${d.properties.name}, ${d.properties.avgprice}`)
     }

//7.-Se pinta el MAPA

        //En este código pintamos el svg del mapa
        const subunitsPath = groupMap
            .selectAll('.subunits')
            .data(features)
            .enter()
            .append('path')
            .attr('d',pathProjection )          //pintamos el mapa
            .style("stroke", "#fff")
            .attr("stroke-width", 0.5)
            .attr('fill', (d) => color(d.properties.avgprice))
            .style("opacity", .8)
            .attr("class", function(d){ return "Barrio" } )
           // .on("mouseover", mouseOver )
            //.on("mouseleave", mouseLeave )
           //.on('click', onclick)  

    //8.-Creación de la leyenda

        const legend = svg.append('g').attr('class', 'legend');

        const scaleLegend = d3.legendColor()
            .scale(color)
            .cells(10)
            .labelFormat(d3.format(".0f"))
            .title("Tramo Precios");
            
        const legendDraw= legend
            .attr("transform", "translate(10 ,50)")
            .style("font-size","10px")
            .style("font-weight","900")
            .call(scaleLegend)

        // d3.legendcolor
        /*const numberOfLegends = 5;

        const scaleLegend = d3.scaleLinear()
            .domain([0, numberOfLegends])
            .range([0, width]);   
    
        }*/


//----------------------------------------------------------------------------------
//DIAGRAMA DE BARRAS

  //En este apartado vamos a pintar el diagrama de barras
//Vamos a leer un csv con los datos ya cocinados. El fichero se va a componer de vecindario, num habitaciones y num propiedades

//1.-Lectura del fichero que dejamos en gist

d3.csv('https://gist.githubusercontent.com/josulazaroversia/1ba865a37ae0745e99dbde8b046393d2/raw/64a4763dfd000acb9c19fd8585f52a416a6c2028/Grafica_barras',
d3.autoType)
.then(function (data) {
    drawBar(data);
});

/*Creamos la funcion drawBar igual q la anterior para pintar el gráfico de barras.
Esta función debería ir fuera de la drawmap, al final lo he dejado así por hacer pruebas
con los eventos pero no he conseguido q funcionara y la he dejado aqui metida, aunq la idea
sería que estuviera en una funcion separada con ambitos separados.*/

function drawBar(data) {

var margin = {top: 20, right: 20, bottom: 50, left: 70},
   widthBar = 550 - margin.left - margin.right,
   heightBar = 550 - margin.top - margin.bottom;

// 2.-Establecemos los ejes y rangos
var x = d3.scaleBand()
.range([0, widthBar])
.padding(0.1);
var y = d3.scaleLinear()
.range([heightBar, 0]);

//3.-Creamos otro svg que contendrá el gráfico de barras

var svg_gr_barra = d3.select("#prueba")
           .append("svg")
           .attr("width", widthBar + margin.left + margin.right)
           .attr("height", heightBar + margin.top + margin.bottom)
           .append("g")
            .attr("transform", 
                   "translate(" + margin.left + "," + margin.top + ")");

   
   
 // 4.-En cada uno de los ejes, le damos las variables que tiene que obtener
 x.domain(data.map(function(d) { return d.Bedrooms; }));
 y.domain([0, d3.max(data, function(d) { return d.Properties; })]);

 //5.- Aplicamos sobre el nuevo svg las propiedades 
 svg_gr_barra.selectAll(".bar")
     .data(data)
     .enter()
     .append("rect")
     .attr("class", "bar")
     .attr("x", function(d) { return x(d.Bedrooms); })
     .attr("width", x.bandwidth())
     .attr("y", function(d) { return y(d.Properties); })
     .attr("height", function(d) { return heightBar - y(d.Properties); })
     .attr("fill", "rgb(243,51,10)");


 // 6.-Añadimos los datos que se visualizan en el eje X
 svg_gr_barra.append("g")
     .attr("transform", "translate(0," + heightBar + ")")
     .call(d3.axisBottom(x)); 
     
// 7.-Agregamos el eje Y
     svg_gr_barra.append("g")
         .call(d3.axisLeft(y));

     
 // 8.-Damos formato al eje X y el titulo que aparecerá
 svg_gr_barra.append("text")             
 .attr("transform",
       "translate(" + (widthBar/2) + " ," + 
                      (heightBar + margin.top +10) + ")")
 .style("text-anchor", "middle")
 .style("font-size","10px")
 .text("Num habitaciones");

 // 9.-Damos formato al eje Y y el titulo que aparecerá
svg_gr_barra.append("text")
 .attr("transform", "rotate(-90)")
 .attr("y", 0 - margin.left)
 .attr("x",0 - (heightBar / 2))
 .attr("dy", "1em")
 .style("text-anchor", "middle")
 .style("font-size","10px")
 .text("Num. propiedades");   

}
}
