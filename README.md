# ModernExplorationVisualization

Tal y cómo se pedía en el enunciado se han creado 2 figuras en el proyecto: 1 mapa y un diagrama de barras. En el caso del mapa se muestran los colores por cada barrio en función del precio medio del barrio,

Por otro lado, en el caso del diagrama de barras, se han cocinado los datos con un barrio en particular, se ha subido al gist y se ha dibujado el diagrama. La parte del filtrado dinámico mediante eventos interactivos no la ha podido terminar y por ello no la he subido.

